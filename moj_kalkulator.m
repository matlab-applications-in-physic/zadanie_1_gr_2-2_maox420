%1
disp('zad1')
h=6.62607015e-34 %Planck constant imported from https://physics.nist.gov/cgi-bin/cuu/Value?h
disp('h/(2*pi)=')
disp(h/(2*pi))

%2
disp('zad2')
disp('sin(30/exp(1))=')
disp(sind(30/exp(1)))

%3
disp('zad3')
disp('0x00123d3/2.455e23=')
disp(hex2dec('00123d3')/2.455e23)

%4
disp('zad4')
disp('sqrt(exp(1)-pi())=')
disp(sqrt(exp(1)-pi()))

%5
disp('zad5')
disp('10th numer after coma in Pi number:')
disp(mod(floor(pi*10^10),10))

%6
disp('zad6')
disp('Number of days passed from my birthday:') %function datenum displays number of days passed from 1-1-1970, so differnce between current date and my birthday gives solution
disp(datenum(date)-datenum(1998,8,4))

%7
disp('zad7')
R=6.37814e6
disp('arctan((exp(sqrt(7)/2-log(R/1e5)))/0xaabb=')
disp(atan((exp(sqrt(7)/2-log(R/1e5)))/hex2dec('aabb'))) %R=6.37814e6 imported from https://pl.wikipedia.org/wiki/Promie%C5%84_Ziemi

%8
disp('zad8')
Na=6.02214076e23 %Na was taken from https://pl.wikipedia.org/wiki/Sta³a_Avogadra#Liczba_Avogadra
disp('Number of atoms in 1/5 micromol of ethanol:')
disp(0.2*Na/1e6)

%9
disp('zad9')
disp('number of promils')
disp(2/9*1/101*1000)
